## Custom RPM and DEB packages builder.
A list of packages can be obtained [here](https://rcrtv.gitlab.io/builder/index.html).

### Packages dependencies

#### Nginx
- gd
- geoip
- libmaxminddb
