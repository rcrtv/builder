#!/usr/bin/env bash

DEBIAN_FRONTEND="noninteractive"

apt-get update \
  && apt-get install -yq software-properties-common \
  && add-apt-repository -y ppa:maxmind/ppa \
  && echo "deb-src http://nginx.org/packages/ubuntu jammy nginx" >>/etc/apt/sources.list.d/nginx.list \
  && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62 \
  && apt-get update \
  && apt-get install -yq dh-make quilt lsb-release debhelper dpkg-dev build-essential autoconf automake pkg-config libtool libpcre3-dev libssl-dev zlib1g-dev libgd-dev libgeoip-dev libmaxminddb-dev libbrotli-dev wget git curl gnupg2 ca-certificates lsb-release \
  && cd /usr/local/src \
  && git clone --recursive https://github.com/leev/ngx_http_geoip2_module \
  && git clone --recursive https://github.com/vozlt/nginx-module-vts \
  && git clone --recursive https://github.com/google/ngx_brotli \
  && apt-get source nginx \
  && apt-get build-dep -yq nginx \
  && cd nginx-1* \
  && sed -ri 's/CFLAGS=""/CFLAGS="-Werror"/g' debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --with-http_image_filter_module/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --with-http_geoip_module/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --add-module=\/usr\/local\/src\/nginx-module-vts/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --add-module=\/usr\/local\/src\/ngx_brotli/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --add-module=\/usr\/local\/src\/ngx_http_geoip2_module/g" debian/rules \
&& cat debian/rules \
  && dpkg-buildpackage -b > /dev/null \
  && mkdir -p ${CI_PROJECT_DIR}/build/ubuntu/22 \
  && cp ../nginx_1*.deb ${CI_PROJECT_DIR}/build/ubuntu/22/. \
  && cp ../nginx_1*.deb ${CI_PROJECT_DIR}/build/ubuntu/22/nginx-latest.deb