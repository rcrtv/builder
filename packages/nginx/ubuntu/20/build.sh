#!/bin/bash

DEBIAN_FRONTEND="noninteractive"
OPENSSL_VERSION="1.1.1h"

apt-get update \
  && apt-get install -yq software-properties-common \
  && add-apt-repository -y ppa:maxmind/ppa \
  && echo "deb-src http://nginx.org/packages/ubuntu bionic nginx" >>/etc/apt/sources.list.d/nginx.list \
  && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62 \
  && apt-get update \
  && apt-get install -yq dh-make quilt lsb-release debhelper dpkg-dev dh-systemd build-essential autoconf automake pkg-config libtool libpcre++-dev libssl-dev libgeoip-dev libgd-dev libmaxminddb-dev libbrotli-dev wget git curl gnupg2 ca-certificates lsb-release \
  && cd /usr/local/src \
  && wget https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz && tar xzf openssl-${OPENSSL_VERSION}.tar.gz \
  && git clone --recursive https://github.com/leev/ngx_http_geoip2_module \
  && git clone --recursive https://github.com/vozlt/nginx-module-vts \
  && git clone --recursive https://github.com/google/ngx_brotli \
  && apt-get source nginx \
  && apt-get build-dep -yq nginx \
  && cd nginx-1* \
  && sed -ri 's/CFLAGS=""/CFLAGS="-Werror"/g' debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --with-openssl=\/usr\/local\/src\/openssl-${OPENSSL_VERSION}/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --with-http_image_filter_module/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --with-http_geoip_module/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --add-module=\/usr\/local\/src\/nginx-module-vts/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --add-module=\/usr\/local\/src\/ngx_brotli/g" debian/rules \
  && sed -ri "/configure --prefix=\/etc\/nginx/s/$/ --add-module=\/usr\/local\/src\/ngx_http_geoip2_module/g" debian/rules \
&& cat debian/rules \
  && dpkg-buildpackage -b > /dev/null \
  && mkdir -p ${CI_PROJECT_DIR}/build/ubuntu/20 \
  && cp ../nginx_1*.deb ${CI_PROJECT_DIR}/build/ubuntu/20/. \
  && cp ../nginx_1*.deb ${CI_PROJECT_DIR}/build/ubuntu/20/nginx-latest.deb
