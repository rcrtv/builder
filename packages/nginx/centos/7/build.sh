#!/bin/bash

OPENSSL_VERSION="1.1.1h"

sed -i s/mirror.centos.org/vault.centos.org/g /etc/yum.repos.d/*.repo
sed -i s/^#.*baseurl=http/baseurl=http/g /etc/yum.repos.d/*.repo
sed -i s/^mirrorlist=http/#mirrorlist=http/g /etc/yum.repos.d/*.repo
sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

cat > /etc/yum.repos.d/nginx.repo << 'EOL'
[nginx]
name=nginx stable repo
baseurl=https://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true

[nginx-source]
name=nginx stable source repo
baseurl=https://nginx.org/packages/centos/$releasever/SRPMS
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-$releasever
module_hotfixes=true
EOL

yum -yq groupinstall "Development Tools" \
  && yum -yq install epel-release \
  && yum -yq install rpmdevtools yum-utils wget git automake autoconf libtool make libgeoip-dev libgd-dev libmaxminddb-devel brotli-devel \
  && cd /usr/local/src \
  && wget https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz && tar xzf openssl-${OPENSSL_VERSION}.tar.gz \
  && git clone --recursive https://github.com/leev/ngx_http_geoip2_module \
  && git clone --recursive https://github.com/vozlt/nginx-module-vts \
  && git clone --recursive https://github.com/google/ngx_brotli \
  && cd /root \
  && rpmdev-setuptree \
  && yumdownloader --source nginx \
  && rpm -Uvh nginx*.src.rpm \
  && yum-builddep -y nginx \
  && cd /root/rpmbuild/SPECS \
  && sed -ri "s/(%define BASE_CONFIGURE_ARGS.+)\"\)$/\1 --with-openssl=\/usr\/local\/src\/openssl-${OPENSSL_VERSION}\"\)/" nginx.spec \
  && sed -ri "s/(%define BASE_CONFIGURE_ARGS.+)\"\)$/\1 --with-http_image_filter_module\"\)/" nginx.spec \
  && sed -ri "s/(%define BASE_CONFIGURE_ARGS.+)\"\)$/\1 --with-http_geoip_module\"\)/" nginx.spec \
  && sed -ri "s/(%define BASE_CONFIGURE_ARGS.+)\"\)$/\1 --add-module=\/usr\/local\/src\/nginx-module-vts\"\)/" nginx.spec \
  && sed -ri "s/(%define BASE_CONFIGURE_ARGS.+)\"\)$/\1 --add-module=\/usr\/local\/src\/ngx_brotli\"\)/" nginx.spec \
  && sed -ri "s/(%define BASE_CONFIGURE_ARGS.+)\"\)$/\1 --add-module=\/usr\/local\/src\/ngx_http_geoip2_module\"\)/" nginx.spec \
  && rpmbuild -bb --quiet nginx.spec \
  && mkdir -p ${CI_PROJECT_DIR}/build/centos/7 \
  && cp ../RPMS/x86_64/nginx-1*.rpm ${CI_PROJECT_DIR}/build/centos/7/. \
  && cp ../RPMS/x86_64/nginx-1*.rpm ${CI_PROJECT_DIR}/build/centos/7/nginx-latest.rpm
